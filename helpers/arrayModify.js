const modifyDate = require('./dateModify')

const modArr = (arr = []) => {
  return arr.map(el => {
    let obj = {}
    for (const key of Object.keys(el)) {
      switch (key) {
        case 'Name':
          obj[key] = el[key]
          break
        case 'Last modified' :
          let newEl = modifyDate(el[key])
          obj[key.replace(' ', '_')] = Date.parse(newEl)
          obj[key.replace(' ', '_') + '_fullDate'] = newEl //for checking && debugging
          break
      }
    }
    return obj
  })
}

module.exports = modArr