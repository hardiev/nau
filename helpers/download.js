const path = require('path')
const fs = require('fs')
const http = require('http');
const axios = require('axios')
const { today } = require('./genNewDownloadDate')
const dataPath = path.join(__dirname, '../public/data/')

const isDir = (dirPath) => {
  try {
    return fs.lstatSync(dirPath).isDirectory()
  } catch (error) {
    return false
  }
}

async function downloadElement(elName, dest) {
  const url = `http://updates.nau.kiev.ua/nau9/ukr/${elName}`
  const pathRes = path.resolve(dest, elName)
  const writer = fs.createWriteStream(pathRes)

  const response = await axios({
    url,
    method: 'GET',
    headers: { 'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36' },
    responseType: 'stream',
    httpAgent: new http.Agent({ keepAlive: true }),
    // timeout: 60000,
    validateStatus: function (status) {
      return status >= 200 && status < 300; // default
    },
    // httpsAgent: new https.Agent({ keepAlive: true })
    // proxy: {
    //   host: '192.168.12.10',
    //   port: 3128,
    //   auth: {
    //     username: '09_gar_vp',
    //     password: 'hardie'
    //   }
    // },

  })
  response.data.pipe(writer)

  return new Promise((resolve, reject) => {
    response.data.on('end', () => {
      resolve(elName)
    })
    response.data.on('error', (err) => {
      writer.end()
      reject(err)
    })
  })

}

const download = (arr) => {
  const todayDate = today()
  console.log('*********', todayDate)
  console.log(arr.length)
  let directoryPath = path.join(dataPath, todayDate.date, '/')
  if (!isDir(directoryPath)) {
    fs.mkdirSync(directoryPath)
  }
  let requests = arr.map(el => downloadElement(el.Name, directoryPath))
  // return Promise.all(requests)
  return Promise.allSettled(requests)
}

module.exports = download