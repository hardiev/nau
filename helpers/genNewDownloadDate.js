const fs = require('fs')
const path = require('path')

const opts = {
  year: 'numeric',
  month: '2-digit',
  day: '2-digit'
}
const today = () => {
  // date: new Date().toLocaleDateString('ru-UA', opts)
  return {date: new Date().toISOString().slice(0, 10), date1: new Date().toISOString()}
}
const filePath = path.join(__dirname, '../public/lastDownloadDate.json')

const genNewDwnldDate = () => {
  fs.writeFile(filePath, JSON.stringify(today()), (err) => {
    if(err) throw err
  })
}

module.exports = {
  genNewDwnldDate,
  today
}