// require modules
const fs = require('fs');
const path = require('path')
const archiver = require('archiver')
const { today } = require('./genNewDownloadDate')


const arch = (response) => {
  const todayDate = today()
  const archDataPath = path.join(__dirname, `../public/data/${todayDate.date}`)
  const outputDataPath = path.join(__dirname, '../public/data/', `${todayDate.date}.zip`)
  // create a file to stream archive data to.
  // var output = fs.createWriteStream(__dirname + '/example.zip');
  var output = fs.createWriteStream(outputDataPath);
  var archive = archiver('zip', {
    zlib: { level: 0 } // Sets the compression level.
  });

  // listen for all archive data to be written
  // 'close' event is fired only when a file descriptor is involved
  output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
    console.log(archDataPath, '---', todayDate)
    console.log(outputDataPath)
    // response.download(__dirname + '/example.zip')
    // response.download(outputDataPath)
    // response.send(__dirname)
  });

  // This event is fired when the data source is drained no matter what was the data source.
  // It is not part of this library but rather from the NodeJS Stream API.
  // @see: https://nodejs.org/api/stream.html#stream_event_end
  output.on('end', function () {
    console.log('Data has been drained');
  });

  // good practice to catch warnings (ie stat failures and other non-blocking errors)
  archive.on('warning', function (err) {
    if (err.code === 'ENOENT') {
      // log warning
    } else {
      // throw error
      throw err;
    }
  });

  // good practice to catch this error explicitly
  archive.on('error', function (err) {
    throw err;
  });

  // pipe archive data to the file
  archive.pipe(output);

  // append files from a sub-directory, putting its contents at the root of archive
  // archive.directory('subdir/', false);
  // archive.directory('/home/hardie/webdev_dir/nauExtractor/public/data/', false);
  archive.directory(archDataPath, false);

  // append files from a glob pattern
  // archive.glob('subdir/*.txt');

  // finalize the archive (ie we are done appending files but streams have to finish yet)
  // 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
  archive.finalize();
}

module.exports = arch
