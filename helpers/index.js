const modArr = require('./arrayModify')
const actArr = require('./actualArray')
const download = require('./download')
const { genNewDwnldDate, today } = require('./genNewDownloadDate')
const arch = require('./archiver')

module.exports = {
  modArr,
  actArr,
  download,
  genNewDwnldDate,
  today,
  arch
}