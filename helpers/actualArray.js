const fs = require('fs')
const path = require('path')

const filePath = path.join(__dirname, '../public/lastDownloadDate.json')

const actualArr = (arr) => {
  let fileContent = JSON.parse(fs.readFileSync(filePath, 'utf8'))
  let actDate = Date.parse(new Date(fileContent.date))
  return arr.filter(el => {
    return el.Last_modified >= actDate
    // return el.Last_modified >= actDate && el.Last_modified <= 1595548800000
  })
}

module.exports = actualArr