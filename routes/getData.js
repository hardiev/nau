var express = require('express');
var router = express.Router();
const fetch = process.env['http_proxy'] && process.env['https_proxy'] ? require('node-fetch-with-proxy') : require('node-fetch')
const cheerio = require('cheerio')
const HtmlTableToJson = require('html-table-to-json');
const { modArr, actArr, download, genNewDwnldDate, arch } = require('../helpers')
const archiver = require('archiver');
const fs = require('fs')


/* GET data. */
router.get('/', function (req, res, next) {
  fetch('http://updates.nau.kiev.ua/nau9/ukr/?C=N;O=D')
    .then(resp => resp.text())
    .then(data => {
      const $ = cheerio.load(data)
      const jsonTable = HtmlTableToJson.parse(`<table>${$('table').html()}</table>`)
      const sourceArray = (jsonTable.results[0]).slice(1, 100)
      const modifiedArray = modArr(sourceArray)
      const actualModifiedArray = actArr(modifiedArray)

      genNewDwnldDate()
      // download(actualModifiedArray)
      return download(actualModifiedArray)

      // arch(res)

      // res.download(__dirname + '/../helpers/example.zip')
    })
    .then(
      responses => {
        console.log(responses)
        arch(res)
        res.send("OK")
      },
      error => console.log(error)
    )
});

module.exports = router;